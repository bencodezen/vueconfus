const fs = require('fs');

const originalScheduleDay0 = require('../content/scheduleDay0.json')
const originalScheduleDay1 = require('../content/scheduleDay1.json')
const originalScheduleDay2 = require('../content/scheduleDay2.json')

// Establish workshop display order
const workshopOrder = [
  'Registration & Breakfast',
  'A Deep Dive into Vue',
  'Proven Patterns for Building Vue Apps',
  'Pumped Up Plugins',
  'Vuex Demystified - Application State Management',
  'Ridiculously Reusable Components in Vue.js',
  'Intro to Vue.js',
  'VueVixens Workshop',
  'Evening Reception'
]

// Create new schedule based on workshopOrder
const organizedDay0 = []

workshopOrder.forEach(workshopName => {
  originalScheduleDay0.filter(workshop => {
    if (workshop.topic.indexOf(workshopName) > -1) {
      organizedDay0.push(workshop)
    }
  })
})

/**
 * Create data files for Vue after sorting data 
 * @param {*} fileName 
 * @param {*} data 
 */
const updateSchedule = (fileName, data) => {
  // Uses rough sorting based on string, not reliable
  // if we use more complicated start times
  let newSchedule = data.sort((a, b) => {
    if (a.startTime < b.startTime) {
      return -1
    } else if (a.startTime > b.startTime) {
      return 1
    } else {
      return 0
    }
  })

  fs.writeFile(`../content/2019/${fileName}`, JSON.stringify(newSchedule), err => {
    if (err) {
      return console.log(err)
    }

    console.log(`${fileName} successfully saved.`)
  })
}

// Call function to create updated schedules
updateSchedule('scheduleDay0.json', organizedDay0)
updateSchedule('scheduleDay1.json', originalScheduleDay1)
updateSchedule('scheduleDay2.json', originalScheduleDay2)
