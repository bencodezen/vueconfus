const fs = require('fs')
const speakerList = require('../content/speakers/speaker-list.js')
const speakerOrder = [
  'Evan You',
  'Sarah Drasner',
  'Damian Dulisz',
  'Natalia Tepluhina',
  'Eduardo',
  "Debbie O'Brien",
  'Gregg Pollack',
  'Sarah Dayan',
  'Henry Zhu',
  'Maria Lamardo',
  'Alexander Lichter',
  'Jaime Jones',
  'Oscar Spencer',
  'Jamena McInteer',
  'Jack Koppa',
  'Cecy Correa',
  'Alec Barrett',
  'Bart Ledoux'
]

const updateSpeakerList = fileName => {
  let orderedSpeakerList = []

  speakerOrder.forEach(speaker => {
    const session = speakerList.find(cfp => {
      return cfp.name === speaker
    })

    orderedSpeakerList.push(session)
  })

  fs.writeFile(
    `../content/speakers/${fileName}`,
    JSON.stringify(orderedSpeakerList),
    err => {
      if (err) {
        return console.log(err)
      }

      console.log(`${fileName} successfully saved.`)
    }
  )
}

updateSpeakerList('speaker-list-2020.json')
