var fs = require('fs'),
    _ = require('underscore');

function jsonEscape(str) {
    return str.replace(/\n/g, "").replace(/\r/g, "").replace(/\t/g, "");
}

function esc(key, val) {
    console.log('encountered string')
    if (typeof (val) != "string") return val;

    return jsonEscape(val);
}

var buffer = '';
var file = fs.readFileSync("data/generic_events_promo.json", "utf8");
file = (JSON.parse(jsonEscape(file)));

var scheduleJson = fs.readFileSync("data/generic_schedule.json", "utf8");
scheduleJson = (JSON.parse(jsonEscape(scheduleJson)));

function speakerData() {
    //  console.log('speaker count:' + file.speakers.speakers.length)

    var counter = 0;
    var allSessions = [];
    console.log('session count:' + file.events.length);
    _.each(file.events, function (e) {
        var speakersInfo = [];
        var thisSession;
        if (e.type == 'lecture' || e.type == 'workshop') {
            _.each(e.persons, function (p) {
                counter++;
                if (p.twitter_name) {
                    var scrubbedTwitterName = p.twitter_name.toLowerCase();
                    if (scrubbedTwitterName) {
                        if (scrubbedTwitterName.indexOf('@') === -1) {
                            // scrubbedTwitterName = '@'+scrubbedTwitterName;
                        }
                        speakersInfo.push({
                            twitter: 'https://twitter.com/' + scrubbedTwitterName,
                            name: p.full_public_name,
                            title: p.abstract,
                            img: 'https://cfp.connectevents.io' + p.avatar_path
                        });
                    }
                } else {

                    console.log('MISSING TWITTER :' + splitNames(p.full_public_name));
                }
            });
            thisSession = {
                sessionTitle: e.title,
                sessionAbstract: e.abstract,
                twitter: speakersInfo[0].twitter,
                name: speakersInfo[0].name,
                title: speakersInfo[0].title,
                img: speakersInfo[0].img
            }
            allSessions.push(thisSession);
        }

    });


    fs.writeFile('vuespeaker-list.json', JSON.stringify(allSessions), function (err) {
        if (err) return console.log(err);
        console.log('allSessions');
    });

    // console.log(allSessions)
}

function scheduleData() {

    _.each(scheduleJson.schedule.conference.days, function (e) {

        var allSessions = [];

        var allRooms = _.map(e.rooms, function(r) {
            return r;
        });
        // console.log(allRooms);
        allRooms = _.flatten(allRooms);

        // console.log(e)
        var thisSession;
        _.each(allRooms, function (session) {
            var speakersInfo = [];
            _.each(session.persons, function (p) {
                // console.log('speaker:' + p)
                if (p.twitter_name) {
                    var scrubbedTwitterName = p.twitter_name.toLowerCase();
                    if (scrubbedTwitterName) {
                        var onePerson = {
                            twitter: 'https://twitter.com/' + scrubbedTwitterName,
                            name: p.full_public_name,
                            abstract: p.abstract,
                            img: 'https://cfp.connectevents.io' + p.avatar_path
                        }
                        speakersInfo.push(onePerson);
                    }
                } else {

                    console.log('MISSING TWITTER :' + splitNames(p.full_public_name));
                }
            });
            if (session.type=='other') {
                thisSession = {
                    startTime: session.start,
                    duration: session.duration,
                    topic: session.title,
                    type: 'talk',
                    img: '/img/ic-happy.svg',
                    description: session.abstract,
                };
            } else {
                console.log(session.title, speakersInfo[0])
                thisSession = {
                    startTime: session.start,
                    duration: session.duration,
                    topic: session.title,
                    type: 'talk',
                    img: speakersInfo[0].img,
                    author: speakersInfo[0].name,
                    authorInfo: speakersInfo[0].abstract,
                    description: session.abstract,
                    social: [
                        {
                            twitter: speakersInfo[0].twitter
                        }
                    ]
                }
            }

            allSessions.push(thisSession);
        });

        fs.writeFile('scheduleDay'+e.index+'.json', JSON.stringify(allSessions), function (err) {
            if (err) return console.log(err);
            console.log('scheduleDay'+e.index+'.json');
        });

    });
    // console.log(allSessions)
}


function splitNames(str) {
    var splitNamesArray = str.split(" ");
    var splitString = "";
    for (var i = 0; i < splitNamesArray.length - 1; i++) {
        splitString += splitNamesArray[i] + " "
    }
    splitString += "," + splitNamesArray[splitNamesArray.length - 1];
    return splitString;
}

speakerData();
scheduleData();

