export default [
  {
    name: 'Early Bird Individual: Conference',
    date: 'May 16-17, 2024',
    price: '$795',
    disabled: false,
    url: 'https://reg.connectevents.io/ConnectEvents/vueconfus2024/'
  },
    {
    name: 'Early Bird: Conference + Workshop',
    date: 'May 15-17, 2024',
    price: '$1395',
    disabled: false,
    url: 'https://reg.connectevents.io/ConnectEvents/vueconfus2024/'
    },
//  {
//    name: 'Early Bird Group: Conference',
//    date: 'May 24-25, 2024',
//    price: '$745',
//    disabled: false,
//    url: 'https://reg.connectevents.io/ConnectEvents/vueconfus2024/'
//  },
//  {
//    name: 'Early Bird Group: Conference + Workshop',
//    date: 'May 23-25, 2024',
//    price: '$1245',
//    disabled: false,
//    url: 'https://reg.connectevents.io/ConnectEvents/vueconfus2024/'
]
