module.exports = [
  {
    sessionTitle: 'TypeScript & Vue @ Politico',
    sessionAbstract:
      '“JavaScript that scales” is the tagline for TypeScript, and it can be a beautiful partner for increasingly complex Vue apps. We’ll discuss how CLI TypeScript Vue projects can increase development speed, decrease onboarding time, type errors, and typos, while encouraging self-documenting & maintainable code. Finally, let’s look at remaining hurdles for Vue’s TS implementation (Vuex, mixins, templates), and how the composition API + Vetur will continue to ease those growing pains.',
    twitter: undefined,
    name: 'Jack Koppa',
    img:
      'https://cfp.connectevents.io/system/avatars/1639/large/jack_koppa_photo.JPG?1572288472',
    id: 5252
  },
  {
    sessionTitle:
      'Demystifying the Vue Lifecycle and other pieces of the Vue instance',
    sessionAbstract:
      'An overview of the Vue lifecycle with examples of when to use each lifecycle method. Also examines various pieces of the Vue instance and how to use them effectively and the pitfalls associated. Examples would be things like v-for key and the pitfalls of just using index, $refs and when to use them, and $nextTick and the problems it can help solve.',
    twitter: undefined,
    name: 'Jaime Jones',
    img:
      'https://cfp.connectevents.io/system/avatars/2344/large/DmHRhUCUYAAlX2d.jpg?1569603863',
    id: 5119
  },
  {
    sessionTitle:
      'Launching a New Design System on an Existing Site with Zero Downtime',
    sessionAbstract:
      'Your task is to launch a brand new design system on an existing high-traffic website without impacting the live site or negatively affecting conversion. In this talk, we’ll share tips on how to achieve a painless website reskin launch through an iterative approach using feature flagging and continuous QA / deployment. Launch day will be as easy as flipping a switch on a new design that has already been thoroughly QA’d in production by key stakeholders.',
    twitter: undefined,
    name: 'Cecy Correa',
    img:
      'https://cfp.connectevents.io/system/avatars/2332/large/cecy_correa_headshot.jpg?1568652125',
    id: 5194
  },
  {
    sessionTitle: 'Opening Keynote with Evan',
    sessionAbstract: 'Opening Keynote with Evan',
    twitter: undefined,
    name: 'Evan You',
    img:
      'https://cfp.connectevents.io/system/avatars/1107/large/499550.jpeg?1515104079',
    id: 5377
  },
  {
    sessionTitle: 'An Animated Guide to Vue 3 Reactivity and Internals',
    sessionAbstract: 'An Animated Guide to Vue 3 Reactivity and Internals',
    twitter: undefined,
    name: 'Sarah Drasner',
    img:
      'https://cfp.connectevents.io/system/avatars/1151/large/97cssCek_400x400.jpg?1513436066',
    id: 5376
  },
  {
    sessionTitle: 'The State of CSS in Vue',
    sessionAbstract:
      "There are a lot of ways to include CSS in your Vue apps, and they all have pros and cons. Knowing which method to choose can be confusing with all the different options available. In this talk, you'll learn about different ways to bring CSS into your Vue app and how to pick a methodology for your project.",
    twitter: undefined,
    name: 'Jamena McInteer',
    img:
      'https://cfp.connectevents.io/system/avatars/2315/large/eightfifteenphotography-womenwhocode-330_450x450.jpg?1571059049',
    id: 5201
  },
  {
    sessionTitle: "Vue 3's Composition API Explained Visually",
    sessionAbstract:
      'The most powerful new feature of Vue 3 is the composition API, which provides an alternative syntax for writing components.  In this talk, Gregg Pollack will introduce why this new API is needed, how it improves upon what is currently possible in Vue, and the basics of its syntax.',
    twitter: undefined,
    name: 'Gregg Pollack',
    img:
      'https://cfp.connectevents.io/system/avatars/1059/large/GreggPollack-Square-New.jpg?1513615639',
    id: 5087
  },
  {
    sessionTitle: 'Validations in the Composition Age',
    sessionAbstract:
      'Vuelidate is a model-based validation library, but thanks to the Composition API that "model" right now is more fluid than ever. But this also opens up several new possibilities which we will explore in this talk.',
    twitter: undefined,
    name: 'Damian Dulisz',
    img:
      'https://cfp.connectevents.io/system/avatars/1087/large/damien.png?1574211784',
    id: 5203
  },
  {
    sessionTitle: 'The Election Map is a Vue App',
    sessionAbstract:
      'In advance of the 2018 midterm elections, the broadcast and digital teams at MSNBC and NBC News undertook a complete rebuild of the interactive big board that on-air talent like Steve Kornacki and Chuck Todd use to tell stories about the election. Alongside designers and developers from TWO-N, the team built a Vue app to display the map of election results across the United States and other visualizations of election data. This talk will describe the app’s architecture, the technical challenges that the team encountered and overcame, and the ongoing efforts to prepare the app for 2020 presidential election coverage.',
    twitter: undefined,
    name: 'Alec Barrett',
    img:
      'https://cfp.connectevents.io/system/avatars/2394/large/alec.jpeg?1574219970',
    id: 5246
  },
  {
    sessionTitle: 'SEO in a Vue.js world',
    sessionAbstract:
      'What do a SaaS landing page, blog and e-commerce store have in common? They should all be found through a search engine, ideally by as many people as possible. Search engine optimization (SEO) can therefore be decisive for the success and also the revenue of your projects.\r\nBut when it comes to Vue.js and SEO, there are many controversial opinions about the impact of Vue on SEO efforts. In my session we will go through the basics of SEO and examine best practices and their implementations with the help of vue-meta and Nuxt.js.',
    twitter: undefined,
    name: 'Alexander Lichter',
    img:
      'https://cfp.connectevents.io/system/avatars/2402/large/lichter.jpg?1571149363',
    id: 5237
  },
  {
    sessionTitle: 'Get the most out of Vue Router',
    sessionAbstract:
      "Routers in Single page applications touch a broad part of our business logic. As a consequence, we often end up with different ways of handling the same pattern/UX/logic in our code and we often wonder which one is better and why. Different ways of handling data fetching that change the user experience, different ways to implement layouts, and many more. During this talk, I will cover very practical implementations that I have found useful in the past and explain the differences between various Vue Router features. After the talk you will have a better understanding of Vue Router's API and hopefully the excitement to refactor some bits of your Vue app!",
    twitter: undefined,
    name: 'Eduardo',
    img:
      'https://cfp.connectevents.io/system/avatars/1097/large/squared.jpeg?1511449030',
    id: 5197
  },
  {
    sessionTitle: 'Documenting components made easy',
    sessionAbstract:
      "Using shared components without proper documentation can be a pain.  Whether you're publishing a component library, or just sharing components with your colleagues, increase the ease of adoption by writing clear documentation.  In this talk, I'll show you how easy it is to write beautiful documentation for your components which other developers will love.",
    twitter: undefined,
    name: 'Bart Ledoux',
    img:
      'https://cfp.connectevents.io/system/avatars/2375/large/bart.jpeg?1574219980',
    id: 5160
  },
  {
    sessionTitle: 'Vue as Compiler',
    sessionAbstract:
      "Frameworks have over the years become more and more like traditional compilers; from pure runtime to more and more compile time. Vue 3 is especially so. Let's talk about how they work and why it's important.",
    twitter: undefined,
    name: 'Henry Zhu',
    img:
      'https://cfp.connectevents.io/system/avatars/2409/large/henryzhu30914.web__copy.jpg?1571190159',
    id: 5251
  },
  {
    sessionTitle: 'Unconventional Vue—Vue as a Backend Framework',
    sessionAbstract:
      'While Vue has emerged as a dominant frontend framework, we can’t forget about the other side of the spectrum. What if we leveraged Vue 3.0’s powerful standalone observability system to manage our backend datastore, with all its reactivity goodness? We could build a highly reactive chat app, power a live scoreboard, or maybe even have Vue trigger AWS Lambda functions as app data changes…\r\n',
    twitter: undefined,
    name: 'Oscar Spencer',
    img:
      'https://cfp.connectevents.io/system/avatars/1719/large/oscar-headshot-square.jpg?1541099235',
    id: 5231
  },
  {
    sessionTitle: 'Test-Driven Development with Vue.js',
    sessionAbstract:
      'Testing a component can be counter-intuitive. It requires a mental shift to understand what to test, and the line between unit and end-to-end tests. TDD makes everything easier. You’re starting from actual specs, a list of things that the component should do, without caring about how it does it.',
    twitter: undefined,
    name: 'Sarah Dayan',
    img:
      'https://cfp.connectevents.io/system/avatars/2300/large/sarah-dayan.jpg?1567414934',
    id: 5061
  },
  {
    sessionTitle: 'Static is the new dynamic',
    sessionAbstract:
      'With Nuxt you can now have full static, that means instead of calling the api on navigation you call the payload, this hugely increases performance. But you can also have a single page application as a fallback meaning the sky is the limit as far as static pages is concerned. ',
    twitter: undefined,
    name: "Debbie O'Brien",
    img:
      'https://cfp.connectevents.io/system/avatars/2380/large/debbie.jpg?1574220071',
    id: 5169
  },
  {
    sessionTitle: "Content Loading That Isn't Broken",
    sessionAbstract:
      "How does Vue.js handle rerouting and loading new content with a screen reader? Let's explore how we can improve the experience for a lot of users who rely on assistive technologies.",
    twitter: undefined,
    name: 'Maria Lamardo',
    img:
      'https://cfp.connectevents.io/system/avatars/1699/large/maria.jpeg?1543892057',
    id: 5115
  },
  {
    sessionTitle: 'All you need is <s>love</s> Apollo Client',
    sessionAbstract:
      'While we usually mention Apollo Client only in connection with GraphQL, it can do a lot more things to your Vue application: accessing REST endpoints, batching queries and replace Vuex in managing application state! In this talk, I will cover these advanced cases while also explaining the basics of using Apollo with GraphQL endpoints',
    twitter: undefined,
    name: 'Natalia Tepluhina',
    img:
      'https://cfp.connectevents.io/system/avatars/1237/large/natalia.jpg?1529377889',
    id: 5082
  }
]
