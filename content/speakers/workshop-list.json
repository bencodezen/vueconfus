[
	{
		"sessionTitle": "A Deep Dive into Vue",
		"sessionAbstract": "In this workshop we are going to do a deep dive into the two fundamental concepts in Vue: reactivity and rendering. We will explore how Vue detects and reacts to changes, and how Vue compiles your templates into virtual DOM render functions. Based on this knowledge, we will then discuss how to unlock the full potential of Vue with advanced component patterns.",
		"twitter": "https://twitter.com/youyuxi",
		"name": "Evan You",
		"title": "Creator of Vue.js ",
		"img": "https://cfp.connectevents.io/system/avatars/1107/large/499550.jpeg?1515104079"
	},
	{
		"sessionTitle": "Proven Patterns for Building Vue Apps",
		"sessionAbstract": "In this workshop, we'll cover everything you need to know to get started building world-class Vue applications. Topics will include configuring Webpack for single-file components, setting up the most advanced workflows currently possible, how to organize (and reorganize) increasingly complex applications, and more.",
		"twitter": "https://twitter.com/chrisvfritz",
		"name": "Chris Fritz",
		"title": "Educator/hacker working to make web dev simpler. Vue core team member and curator of our docs.",
		"img": "/img/chris.jpg"
	},
	{
		"sessionTitle": "Ridiculously Reusable Components in Vue.js",
		"sessionAbstract": "Components are the bread and butter of most modern front-end frameworks, such as Vue.js. They help you structure and manage your application by glueing together your application logic and the interface. However, as an application grows, its components tend to get bigger and harder to reason about.\r\nThis workshop will demonstrate how to efficiently design your components by focusing on maximum flexibility while avoiding premature optimisation. Throughout the workshop, we’ll build a set of components for a preexisting application. Following the evolving requirements, we will then perform a series of refactoring cycles to explore different possible solutions and patterns/anti-patterns.\r\nWe’ll also talk about good practices for managing responsibilities of our components. By the end of the workshop, you should be able to write future-proof components that are easy to work with and resilient to changes.",
		"twitter": "https://twitter.com/damiandulisz",
		"name": "Damian Dulisz",
		"img": "/img/damian.jpg"
	},
	{
		"sessionTitle": "Ridiculously Reusable Components in Vue.js",
		"sessionAbstract": "Components are the bread and butter of most modern front-end frameworks, such as Vue.js. They help you structure and manage your application by glueing together your application logic and the interface. However, as an application grows, its components tend to get bigger and harder to reason about.\r\nThis workshop will demonstrate how to efficiently design your components by focusing on maximum flexibility while avoiding premature optimisation. Throughout the workshop, we’ll build a set of components for a preexisting application. Following the evolving requirements, we will then perform a series of refactoring cycles to explore different possible solutions and patterns/anti-patterns.\r\nWe’ll also talk about good practices for managing responsibilities of our components. By the end of the workshop, you should be able to write future-proof components that are easy to work with and resilient to changes.",
		"twitter": "https://twitter.com/bencodezen",
		"name": "Ben Hong",
		"title": "Ben Hong’s origin story harkens back to the days when he was a kid building random sites like “Fortune Cookie Universe” on free hosting sites like Angelfire and Geocities. Unbeknownst to him, he was unaware that he could have made a career of it for many years. Shortly after becoming an I/O Psychologist though, he managed to find his way back to his roots and is currently a Senior UI Developer at Politico.",
		"img": "https://cfp.connectevents.io/system/avatars/1121/large/ben_profile_square.jpg?1528051667"
	},
	{
		"sessionTitle": "Vuex Demystified - Application State Management",
		"sessionAbstract": "In Vue.js is quite simple to work with components, and share data among them using events and props - especially in small to medium projects. When an application starts growing, managing components state might become tedious and overwhelming. This is where Vuex comes to our rescue. Vuex is a state management pattern and official library for Vue.js applications. It serves as a centralized store for all the components in an application, with rules ensuring that the state can only be mutated in a predictable fashion.\r\n\r\nBy the end of the workshop, attendees will be able to develop elegant, debuggable, maintainable, and organized applications using a centralized store. No prior experience with Vuex is needed.\r\n\r\nWe will answer the common question “When should I use Vuex?” and work on real-world features.\r\n\r\nOutline\r\nUnderstanding State Management Patterns\r\n What is Vuex and what problems solves\r\n Core concepts including Mutations, Actions, and Getters\r\n Modularising the store\r\n Composing Vuex assets using Higher-Order Functions\r\n Implementing Vuex pseudo-cache\r\n Best practices, common gotchas, and application architecture",
		"twitter": "https://twitter.com/hootlex",
		"name": "Alex Kyriakidis",
		"title": "Vue Core Member and creator of VueSchool.io and The Vue.js Master Class",
		"img": "https://cfp.connectevents.io/system/avatars/1765/large/alex.jpg?1543791438"
	},
	{
		"sessionTitle": "Vuex Demystified - Application State Management",
		"sessionAbstract": "In Vue.js is quite simple to work with components, and share data among them using events and props - especially in small to medium projects. When an application starts growing, managing components state might become tedious and overwhelming. This is where Vuex comes to our rescue. Vuex is a state management pattern and official library for Vue.js applications. It serves as a centralized store for all the components in an application, with rules ensuring that the state can only be mutated in a predictable fashion.\r\n\r\nBy the end of the workshop, attendees will be able to develop elegant, debuggable, maintainable, and organized applications using a centralized store. No prior experience with Vuex is needed.\r\n\r\nWe will answer the common question “When should I use Vuex?” and work on real-world features.\r\n\r\nOutline\r\nUnderstanding State Management Patterns\r\n What is Vuex and what problems solves\r\n Core concepts including Mutations, Actions, and Getters\r\n Modularising the store\r\n Composing Vuex assets using Higher-Order Functions\r\n Implementing Vuex pseudo-cache\r\n Best practices, common gotchas, and application architecture",
		"twitter": "https://twitter.com/hootlex",
		"name": "Rolf Haug",
		"title": "Co-founder and educator at VueSchool.io",
		"img": "/img/rolf.jpg"
	},
	{
		"sessionTitle": "Intro to Vue.js",
		"sessionAbstract": "This workshop will teach you the fundamentals of Vue.js so you can start being productive and building apps immediately. With visual animations and analogies that make abstract concepts concrete, along with coding challenges that help you learn by doing, you'll have a solid foundation of Vue skills by the end of the day. Brought to you by the teachers at VueMastery.com.",
		"twitter": "https://twitter.com/greggpollack",
		"name": "Gregg Pollack",
		"title": "Gregg Pollack is the founder of Vue Mastery, the ultimate learning resource for Vue Developers.  Previously he founded Code School, an online software programming school which was acquired by Pluralsight, Envy, a web application development firm, and Starter Studio, Orlando's first technical accelerator now in its eighth class.  He’s passionate about being a father, teaching programming, traveling the world, organizational psychology, and attending theater.",
		"img": "https://cfp.connectevents.io/system/avatars/1059/large/GreggPollack-Square-New.jpg?1513615639"
	},
	{
		"sessionTitle": "Intro to Vue.js",
		"sessionAbstract": "This workshop will teach you the fundamentals of Vue.js so you can start being productive and building apps immediately. With visual animations and analogies that make abstract concepts concrete, along with coding challenges that help you learn by doing, you'll have a solid foundation of Vue skills by the end of the day. Brought to you by the teachers at VueMastery.com.",
		"twitter": "https://twitter.com/AdamJahr",
		"name": "Adam Jahr",
		"title": "Adam Jahr is co-founder and instructor over at VueMastery.com, where he releases weekly Vue.js video tutorials and articles. He also hosts the Official Vue News podcast, covering the latest news and libraries in the Vue.js ecosystem.",
		"img": "/img/adam.jpg"
	},
	{
		"sessionTitle": "VueVixens Workshop",
		"sessionAbstract": "VueVixens workshop on web/mobile development with VueJS",
		"twitter": "https://twitter.com/jenlooper",
		"name": "Jen Looper",
		"title": "Jen Looper is a Senior Developer Advocate at Progress with over 15 years' experience as a web and mobile developer, specializing in creating cross-platform mobile apps. She's a multilingual multiculturalist with a passion for hardware hacking, mobile apps, machine learning and discovering new things every day. Visit her online at http://www.ladeezfirstmedia.com, or via Twitter @jenlooper.",
		"img": "https://pbs.twimg.com/profile_images/1085627202815881216/pGPLZ7HN_400x400.jpg"
	},
	{
		"sessionTitle": "VueVixens Workshop",
		"sessionAbstract": "VueVixens workshop on web/mobile development with VueJS",
		"twitter": "https://twitter.com/cotufa82",
		"name": "Diana Rodriguez",
		"title": "Worldwide Community Organizer",
		"img": "/img/diana.jpg"
	}
]
