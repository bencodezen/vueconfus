module.exports = {
  build: {
    vendor: ['content.plugin'],
    extend (config, { isDev, isClient }) {

      config.node = {
        fs: 'empty'
      }
    },
    filenames: {
      // TMP: Increment this each time when doing a release to bust the cache
      app: 'app.' + Date.now() + '.js'
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Vueconf.US',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', content: "Official Vue.js Conference USA" },
      { name: 'description', content: 'Official Vue.js Conference USA' },
      { name: 'keywords', content: 'Vue.js,Vue,HTML,CSS,XML,JavaScript' },
      { property: 'og:title', content: 'Vueconf US 2024 – May 15-17 in New Orleans, LA | Vue.js Conference' },
      { property: 'og:site_name', content: 'Vueconf US 2024 – May 15-17 in New Orleans, LA | Vue.js Conference' },
      { property: 'og:url', content: 'https://vueconf.us' },
      { property: 'og:type', content: 'website' },
      { property: 'og:description', content: 'Vueconf US 2024 – May 15-17 in New Orleans, LA | Vue.js Conference' },
      { property: 'og:image', content: 'https://res.cloudinary.com/connectjs/image/upload/v1702956496/twittercard2024_2-01_faek7d.png' },
      { property: 'twitter:card', content: 'summary_large_image' },
      { property: 'twitter:site', content: '@vueconfus' },
      { property: 'twitter:image', content: 'https://res.cloudinary.com/connectjs/image/upload/v1702956496/twittercard2024_2-01_faek7d.png' },
      { property: 'twitter:title', content: 'Vueconf US 2024 – May 15-17 in New Orleans, LA | Vue.js Conference' },
      { property: 'twitter:description', content: 'Official Vue.js Conference USA' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'theme-color', content: '#3EB882' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/logo-48.png' },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&amp;subset=latin-ext' },
    ],
    script: [
      { type: 'text/javascript', src: '/sw.js' }
    ]
  },
  /*
  ** Global CSS
  */
  // css: ['~assets/css/style.css'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  plugins: [
    { src: '~plugins/analytics', ssr: false },
    { src: '~plugins/cookies', ssr: false },
    { src: '~plugins/offline.js', ssr: false },
    { src: '~plugins/content.plugin.js'}
  ],
  modules: [
    // With options
    ['nuxt-linkedin-pixel-module', {
      /* module options */
      partnerId: '973050',
      disabled: false
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-53526024-1'
    }],
      ['@nuxtjs/moment']
  ],
  router: {
    scrollBehavior(to, from, savedPosition) {
      // if the returned position is falsy or an empty object,
      // will retain current scroll position.
      let position = false

      // if no children detected
      if (to.matched.length < 2) {
        // scroll to the top of the page
        position = { x: 0, y: 0 }
      } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
        // if one of the children has scrollToTop option set to true
        position = { x: 0, y: 0 }
      }

      // savedPosition is only available for popstate navigations (back button)
      if (savedPosition) {
        position = savedPosition
      }

      return new Promise(resolve => {
        // wait for the out transition to complete (if necessary)
        window.$nuxt.$once('triggerScroll', () => {
          // coords will be used if no selector is provided,
          // or if the selector didn't match any element.
          if (to.hash && document.querySelector(to.hash)) {
            // scroll to anchor by returning the selector
            position = { selector: to.hash }
          }
          resolve(position)
        })
      })
    }
  }
}
