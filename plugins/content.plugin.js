import speakerJson from '~/content/2023/speakers.json';
import sessionsJson from '~/content/2023/sessions.json';
import schedule from '~/content/2023/schedule.json';
import moment from 'moment';


function findSpeakerById(speakerId) {
    let speakerObject = speakerJson.find(  e  => {
        if (e.id == speakerId) return e;
    });
    if (speakerObject!=null) {
        speakerObject.twitter = "https://twitter.com/"+speakerObject.questionAnswers[1].answer;
    }
    return speakerObject;
}

function findSessionById(sessionId) {
    let sessionObject = sessions.find(  e  => {
        if (e.id == sessionId) return e;
    });
    // if (speakerObject!=null) {
    //     speakerObject.twitter = "https://twitter.com/"+speakerObject.questionAnswers[1].answer;
    // }
    return sessionObject;
}

function workshopsOnly() {

    let workshops = sessions.filter( e => e.categories[0].categoryItems[0].name === "Workshop");
    return workshops;

}


let tracks = [];
let sessionsByTrack = {};
let sessions = [];
let scheduleDay0= [];
let    scheduleDay1= [];
let    scheduleDay2= [];

sessionsJson.forEach((sessionsHolder) => {

    // console.log(sessionsHolder.groupName);
    sessionsHolder.sessions.forEach((e) => {

        sessions.push(e);
    })
})

sessions.forEach((element) => {
    if (!(element.categories === undefined || element.categories.length == 0)) {
        element.format = element.categories[0].categoryItems[0].name;
        element.track = element.categories[1].categoryItems[0].name;
        if (tracks.includes(element.track)) {

        } else {
            tracks.push(element.track);
            sessionsByTrack[element.track] = [];
        }

        sessionsByTrack[element.track].push(element);
    }

})

speakerJson.forEach((speakerHolder) => {

    // console.log(sessionsHolder.groupName);
    speakerHolder.sessions.forEach((e) => {
        let fullSession = findSessionById(e.id);
        if (!(fullSession.categories === undefined || fullSession.categories.length == 0)) {

            speakerHolder.track = fullSession.categories[1].categoryItems[0].name;
        }
    })
})
scheduleDay0 = schedule[0];
scheduleDay1 = schedule[1];
scheduleDay2 = schedule[2];

function buildScheduleData(data) {


    let newdata = []
        data.rooms.forEach((room) => {
            room.sessions.forEach((element) => {
                newdata.push(reformatScheduleData(element));
            })
        })
    return newdata;
}

function reformatScheduleData(data) {

    let speakerImg = "/img/ic-happy.svg";


    // console.log(data.speakers);
    if (!(data.speakers === undefined || data.speakers.length == 0)) {
        speakerImg = findSpeakerById(data.speakers[0].id);
        speakerImg = speakerImg.profilePicture;
    } else {

    }
    let formattedDate = moment(data.startsAt).format('h:mm')
    return {
        "startTime": formattedDate,
        // "duration": "00:30",
        "topic":  data.title,
        "type": "talk",
        "img": speakerImg,
        "description": data.description,
        id: data.id
    }
}

scheduleDay0 = buildScheduleData(scheduleDay0);
scheduleDay1 = buildScheduleData(scheduleDay1);
scheduleDay2 = buildScheduleData(scheduleDay2);

// console.log(scheduleDay0)

const content = {
    speakerJson,
    sessions,
    findSpeakerById,
    workshopsOnly,
    findSessionById,
    sessionsByTrack,
    tracks,
    scheduleDay0,
    scheduleDay1,
    scheduleDay2,
}

export default ({ app }, inject) => {
    inject('content', content)
}
//
// Vue.use(content);
// export default content;
